﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TPCommand.Receivers {

    [Table("BankAccounts")]
    public class BankAccount {

        [Key]
        public int Id { get; set; }
        public decimal Balance { get; set; }
        public int OwnerId { get; set; }

        [ForeignKey(nameof(OwnerId))]
        public virtual User Owner { get; set; }

        public BankAccount() { }

        public BankAccount(int id, User owner, decimal balance) {
            Id = id;
            Owner = owner;
            Balance = balance;
        }
    }
}
