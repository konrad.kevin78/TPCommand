﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TPCommand.Receivers {

    [Table("Users")]
    public class User {

        [Key]
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public virtual List<BankAccount> BankAccounts { get; set; }

        public User() { }

        public User(int id, string firstname, string lastname, List<BankAccount> bankAccounts) {
            Id = id;
            Firstname = firstname;
            Lastname = lastname;
            BankAccounts = bankAccounts;
        }

        public void DisplayBankAccounts() {
            foreach (BankAccount account in BankAccounts) {
                Console.WriteLine($"[Account {account.Id}] Current balance: {account.Balance} euros");
            }

            Console.WriteLine();
        }

        public void DisplayInformation() {
            Console.WriteLine($"Firstname: {Firstname}");
            Console.WriteLine($"Lastname: {Lastname}");
            Console.WriteLine();
        }
    }
}
