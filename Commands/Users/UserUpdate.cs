﻿using System;
using TPCommand.Receivers;

namespace TPCommand.Commands.Users {
    public class UserUpdate : ICommand {
        private readonly BankContext _context;
        private readonly User _user;
        private readonly User _oldUser;
        private readonly User _newUser;

        public UserUpdate(User user, String firstname, String lastname) {
            _context = BankContext.GetInstance();
            _user = user;
            _oldUser = new User(user.Id, user.Firstname, user.Lastname, user.BankAccounts);
            _newUser = new User(user.Id, firstname, lastname, user.BankAccounts);
        }

        public bool Execute() {
            if (_user == null || _oldUser == null || _newUser == null) {
                Console.WriteLine("Invalid user or values.");
                return false;
            }

            _user.Firstname = _newUser.Firstname;
            _user.Lastname = _newUser.Lastname;
            _context.SaveChanges();
            Console.WriteLine($"User {_oldUser.Firstname} {_oldUser.Lastname} successfully update to {_user.Firstname} {_user.Lastname}.");
            return true;
        }

        public bool Undo() {
            if (_user == null || _oldUser == null || _newUser == null) {
                Console.WriteLine("Invalid user or values.");
                return false;
            }

            _user.Firstname = _oldUser.Firstname;
            _user.Lastname = _oldUser.Lastname;
            _context.SaveChanges();
            Console.WriteLine("User update undone.");
            return true;
        }
    }
}
