﻿using System;
using TPCommand.Receivers;

namespace TPCommand.Commands.Users {
    public class UserDelete : ICommand {
        private readonly BankContext _context;
        private readonly User _user;

        public UserDelete(User user) {
            _context = BankContext.GetInstance();
            _user = user;
        }

        public bool Execute() {
            if (_user == null) {
                Console.WriteLine("Invalid user.");
                return false;
            }

            _context.Users.Remove(_user);
            _context.SaveChanges();
            Console.WriteLine($"User {_user.Firstname} {_user.Lastname} successfully deleted.");
            return true;
        }

        public bool Undo() {
            if (_user == null) {
                Console.WriteLine("Invalid user.");
                return false;
            }

            _context.Users.Add(_user);
            _context.SaveChanges();
            Console.WriteLine($"User deletion undone.");
            return true;
        }
    }
}
