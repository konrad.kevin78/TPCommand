﻿namespace TPCommand.Commands {
    interface ICommand {
        bool Execute();
        bool Undo();
    }
}
