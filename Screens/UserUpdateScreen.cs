﻿using System;
using TPCommand.Commands.Users;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    public class UserUpdateScreen {
        private readonly User _user;

        public UserUpdateScreen(User user) {
            _user = user;
        }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== USER UPDATE ==========");

            _user.DisplayInformation();

            String firstname = ScreenUtils.GetStringInput("Enter your new firstname: ");
            String lastname = ScreenUtils.GetStringInput("Enter your new lastname"); ;

            UserUpdate userUpdateCommand = new UserUpdate(_user, firstname, lastname);
            CommandsManager commandsManager = CommandsManager.GetInstance();
            commandsManager.Do(userUpdateCommand);
        }
    }
}
