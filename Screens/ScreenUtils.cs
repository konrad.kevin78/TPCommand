﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    class ScreenUtils {
        public static decimal GetDecimalInput(String text) {
            bool parsed = false;
            decimal result;

            do {
                Console.Write(text);
                string input = Console.ReadLine();
                parsed = Decimal.TryParse(input, out result);
            } while (!parsed);

            return result;
        }

        public static BankAccount GetBankAccountInput(User user, String text) {
            bool parsed = false;
            BankAccount result;

            do {
                Console.Write(text);
                string input = Console.ReadLine();
                parsed = Int32.TryParse(input, out int id);
                result = user.BankAccounts.Find(a => a.Id == id);
            } while (result == null);

            return result;
        }

        public static User GetUserInput(String text) {
            bool parsed = false;
            string input;
            User result;

            do {
                Console.Write(text);
                input = Console.ReadLine();
                parsed = Int32.TryParse(input, out int id);
                result = BankContext.GetInstance().Users.Include(a => a.BankAccounts).Where(a => a.Id == id).FirstOrDefault();
            } while (result == null && input.ToLower() != "new");

            return result;

        } 

        public static int GetIntegerInput(String text) {
            bool parsed = false;
            int result;

            do {
                Console.Write(text);
                string input = Console.ReadLine();
                parsed = Int32.TryParse(input, out result);
            } while (!parsed);

            return result;
        }

        public static String GetStringInput(String text) {
            Console.Write(text);
            string input = Console.ReadLine();
            return input;
        }
    }
}
