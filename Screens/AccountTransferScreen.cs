﻿using System;
using TPCommand.Commands.BankAccounts;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    public class AccountTransferScreen {
        private readonly User _user;

        public AccountTransferScreen(User user) {
            _user = user;
        }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== BANK TRANSFER ACCOUNT ==========");

            if (_user.BankAccounts == null || _user.BankAccounts.Count == 0) {
                Console.WriteLine("You don't have any bank account yet, please create one first.");
                return;
            }

            _user.DisplayBankAccounts();

            BankAccount fromAccount = ScreenUtils.GetBankAccountInput(_user, "Enter the ID of the source account: ");
            BankAccount toAccount = ScreenUtils.GetBankAccountInput(_user, "Enter the ID of the target account: ");
            decimal amount = ScreenUtils.GetDecimalInput("Select the amount you want to transfer (euros): ");

            BankAccountTransfer bankAccountTransferCommand = new BankAccountTransfer(fromAccount, toAccount, amount);
            CommandsManager commandsManager = CommandsManager.GetInstance();
            commandsManager.Do(bankAccountTransferCommand);
        }
    }
}
