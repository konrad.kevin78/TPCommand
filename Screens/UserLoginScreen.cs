﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    class UserLoginScreen : IScreen {
        private BankContext _context;

        public UserLoginScreen() {
            _context = BankContext.GetInstance();
        }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== LOGIN ==========");
            Console.WriteLine("Welcome to your bank system. Here is the list of registered users:");

            DisplayUsers();
            User user = ScreenUtils.GetUserInput("Please enter your customer ID or 'NEW' if you want to register as a new client: ");

            if (user == null) {
                DisplayRegisterScreen();
            } else {
                DisplayUserMenu(user);
            }

            Show();
        }

        private void DisplayUsers() {
            foreach (User user in _context.Users) {
                Console.WriteLine($"[User {user.Id} ({user.Firstname} {user.Lastname})]");
            }

            Console.WriteLine();
        }

        private void DisplayRegisterScreen() {
            UserRegisterScreen nextScreen = new UserRegisterScreen();
            nextScreen.Show();
        }

        private void DisplayUserMenu(User user) {
            UserMenuScreen nextScreen = new UserMenuScreen(user);
            nextScreen.Show();
        }
    }
}
