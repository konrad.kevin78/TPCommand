﻿using System;
using System.Collections.Generic;
using TPCommand.Commands.Users;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    public class UserRegisterScreen : IScreen {
        public UserRegisterScreen() { }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== REGISTER ==========");

            int id = new Random().Next(0, 999999);
            string firstname = ScreenUtils.GetStringInput("Enter your firstname: ");
            string lastname = ScreenUtils.GetStringInput("Enter your lastname: ");
            User user = new User(id, firstname, lastname, new List<BankAccount>());

            UserCreate userCreateCommand = new UserCreate(user);
            CommandsManager commandsManager = CommandsManager.GetInstance();
            commandsManager.Do(userCreateCommand);
        }
    }
}
