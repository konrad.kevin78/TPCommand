﻿using System;
using TPCommand.Commands.BankAccounts;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    public class AccountDepositScreen {
        private readonly User _user;

        public AccountDepositScreen(User user) {
            _user = user;
        }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== BANK DEPOSIT ACCOUNT ==========");

            if (_user.BankAccounts == null || _user.BankAccounts.Count == 0) {
                Console.WriteLine("You don't have any bank account yet, please create one first.");
                return;
            }

            _user.DisplayBankAccounts();

            BankAccount bankAccount = ScreenUtils.GetBankAccountInput(_user, "Enter the ID of the bank account you want to deposit on: ");
            decimal amount = ScreenUtils.GetDecimalInput("Select the amount you want to deposit (euros): ");

            BankAccountDeposit bankAccountDepositCommand = new BankAccountDeposit(bankAccount, amount);
            CommandsManager commandsManager = CommandsManager.GetInstance();
            commandsManager.Do(bankAccountDepositCommand);
        }
    }
}
