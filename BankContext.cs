﻿using Microsoft.EntityFrameworkCore;
using TPCommand.Receivers;

namespace TPCommand {
    public class BankContext : DbContext {
        private static BankContext _context;
        public DbSet<User> Users { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }

        private BankContext() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlite("Data Source=bank.db");
        }

        public static BankContext GetInstance() {
            if (_context == null) {
                _context = new BankContext();
            }

            return _context;
        }
    }
}
