﻿using System;
using System.Collections.Generic;
using TPCommand.Commands;

namespace TPCommand {
    class CommandsManager {
        private static CommandsManager _commandsManager;
        private readonly List<ICommand> _commands = new List<ICommand>();
        private ICommand _last;

        private CommandsManager() { }

        public void Do(ICommand command) {
            bool success = command.Execute();
            if (success) {
                _commands.Add(command);
                _last = command;
            }
        }

        public static CommandsManager GetInstance() {
            if (_commandsManager == null) {
                _commandsManager = new CommandsManager();
            }

            return _commandsManager;
        }

        public int GetCommandsCount() {
            return _commands.Count;
        }

        public void UndoLast() {
            if (_commands.Count == 0) {
                Console.WriteLine("No command to undo");
                return;
            }

            ICommand last = _commands[_commands.Count - 1];
            bool success = last.Undo();
            if (success) {
                _commands.Remove(last);
                _last = last;
            }
        }

        public void RedoLast() {
            if (_last == null) {
                Console.WriteLine("No command to redo");
                return;
            }

            Do(_last);
        }

        public void UndoAll() {
            if (_commands.Count == 0) {
                Console.WriteLine("No command to undo");
                return;
            }

            for (int i = _commands.Count - 1; i >= 0; i--) {
                bool success = _commands[i].Undo();
                if (success) {
                    _last = _commands[i];
                    _commands.RemoveAt(i);
                }
            }
        }
    }
}
