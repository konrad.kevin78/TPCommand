﻿using TPCommand.Screens;

namespace TPCommand {
    class Program {
        static void Main(string[] args) {
            UserLoginScreen loginScreen = new UserLoginScreen();
            loginScreen.Show();
        }
    }
}
